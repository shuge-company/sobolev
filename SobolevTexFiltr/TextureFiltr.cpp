﻿// ConsoleApplication1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#define  _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <conio.h>
#include <string>
#include <omp.h>
#include <sstream>
#include <xmmintrin.h>
#include <pmmintrin.h>
#include <smmintrin.h>
#include <immintrin.h>


#include "MU16Data.h"


//================================================================
class BTexture
{
public:
	BTexture();

	void Create(uint8_t *a_pVarRow, int a_Win_cen);
	void CalcHist();
	double Calc_Mean();


	uint8_t *m_pVarRow;
	uint16_t hist[256];
	int Win_cen;
	int Win_size;
	int Win_Lin;
	double Size_obratn;
};
//================================================================

BTexture::BTexture()
{
	m_pVarRow = nullptr;
}

void BTexture::Create(uint8_t *a_pVarRow, int a_Win_cen)
{
	Win_cen = a_Win_cen;
	Win_size = (Win_cen << 1) + 1;
	Win_Lin = Win_size * Win_size;
	Size_obratn = 1.0 / Win_Lin;
	m_pVarRow = a_pVarRow;
}

void BTexture::CalcHist()
{
	memset(hist, 0, sizeof(uint16_t) * 256);

	for (int i = 0; i < Win_Lin; i++)
	{
		hist[m_pVarRow[i]]++;
	}
}

double BTexture::Calc_Mean()
{
	CalcHist();
	double Sum = 0.0;
	for (int i = 0; i < Win_Lin; i++)
	{
		Sum += m_pVarRow[i] * hist[m_pVarRow[i]] * Size_obratn;
	}
	return Sum;
}
//================================================================
//================================================================

void TextureFiltr_Mean(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
	int i = (Win_cen << 1) + 1;
	int Win_lin = i * i;
	uint8_t *pVarRow = new uint8_t[Win_lin];
	BTexture bt;
	bt.Create(pVarRow, Win_cen);

	int64_t iRow, iCol, iRowMax = ar_cmmIn.m_i64H - Win_cen, iColMax = ar_cmmIn.m_i64W - Win_cen;
	ar_cmmOut.iCreate(ar_cmmIn.m_i64W, ar_cmmIn.m_i64H, 6);

	// Обнуляем края. Первые строки.
	for (iRow = 0; iRow < Win_cen; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);

	int ln, rw;
	for (iRow = Win_cen; iRow < iRowMax; iRow++)
	{
		float *pfRowOut = ar_cmmOut.pfGetRow(iRow);
		// Обнуляем края. Левый край.
		memset(pfRowOut, 0, sizeof(float) * Win_cen);

		for (iCol = Win_cen; iCol < iColMax; iCol++)
		{
			int i_num = 0;
			for (rw = -Win_cen; rw <= Win_cen; rw++)
			{
				uint16_t *pRowIn = ar_cmmIn.pu16GetRow(iRow + rw);
				for (ln = -Win_cen; ln <= Win_cen; ln++)
				{
					double d = (pRowIn[iCol + ln] - pseudo_min)*kfct;
					if (d <= 0.)
						pVarRow[i_num] = 0;
					else
					{
						int iVal = static_cast<int>(d + 0.5);
						if (iVal > 255)
							pVarRow[i_num] = 255;
						else
							pVarRow[i_num] = static_cast<uint8_t>(iVal);
					}
					i_num++;
				}
			}
			pfRowOut[iCol] = static_cast<float>(bt.Calc_Mean());
		}

		// Обнуляем края. Правый край.
		memset(pfRowOut + iColMax, 0, sizeof(float) * (ar_cmmOut.m_i64LineSizeEl - iColMax));
	}

	// Обнуляем края. Последние строки.
	for (iRow = iRowMax; iRow < ar_cmmOut.m_i64H; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);

	if (nullptr != pVarRow)
		delete[] pVarRow;
}
void TextureFiltr_Mean_OMP(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct,int a_iThreads);

void TextureFiltr_Mean_V2(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
void TextureFiltr_Mean_V2_1(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
void TextureFiltr_Mean_V3(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
void TextureFiltr_Mean_V4(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
void TextureFiltr_Mean_V5(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
void TextureFiltr_Mean_V6(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
void TextureFiltr_Mean_V7(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
void TextureFiltr_Mean_V8(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
void TextureFiltr_Mean_V8_3(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
void TextureFiltr_Mean_V8_3_21(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
void TextureFiltr_Mean_V8_3_sse(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
void TextureFiltr_Mean_V8_21_sse(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
void TextureFiltr_Mean_V8_sse4_3_21(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
//void TextureFiltr_Mean_V8_sse4_3_21(MU16Data &ar_cmmIn, MFData &ar_cmmOut, int Win_cen, double pseudo_min, double kfct);
//================================================================
//================================================================
//

int iCodeGenRowsCols_V8(FILE* pf, int Win_cen, int iFlagRows, int iFlagCols)
{
	int Win_size = (Win_cen << 1) + 1;
	int Win_lin = Win_size * Win_size;
	const char* pcAdd_iCmin = "";
	const char* pcAdd_iCminOr0 = "";
	const char* pcCurr = "";
	const char* pcCorrSum = "";
	const char* pcAll = "";
	if (0 != iFlagRows)
	{
		pcCorrSum = " - u32ValueSub * ((static_cast<uint32_t>(pHistSub[i]--) << 1) - 1)";
		if (1 != iFlagCols)
			pcAll = "All";
	}
	fprintf(pf, "\n");
	if (0 == iFlagCols)
	{
		fprintf(pf, " // Первые [0, 2*Win_cen[ колонки\n");
		fprintf(pf, " for (iCol = 0; iCol < (Win_cen << 1); iCol++)\n");
		fprintf(pf, " {\n");
		fprintf(pf, " iCmin = max(Win_cen, iCol - Win_cen);\n");
		fprintf(pf, " iCmax = iCol + Win_cen;\n");
		fprintf(pf, "\n");
	}
	else if (1 == iFlagCols)
	{
		pcAdd_iCmin = " + iCmin";
		pcCurr = "Curr";
		pcAdd_iCminOr0 = "0";
		fprintf(pf, " // Средние [2*Win_cen, ar_cmmIn.m_i64W - Win_size] колонки\n");
		fprintf(pf, " puiSumCurr = puiSum + Win_cen;\n");
		if (0 != iFlagRows)
		{
			fprintf(pf, " pHist = pHistAll + (iCol - Win_cen);\n");
			fprintf(pf, " for (; iCol <= iColMax; iCol++, puiSumCurr++, pHist++)\n");
		}
		else
			fprintf(pf, " for (; iCol <= iColMax; iCol++, puiSumCurr++)\n");
		fprintf(pf, " {\n");
		if (0 == iFlagRows)
		{
			fprintf(pf, " iCmin = iCol - Win_cen;\n");
			fprintf(pf, "\n");
		}
	}else
	{
	pcAdd_iCminOr0 = "iCmin";
	fprintf(pf, " // Последние [ar_cmmIn.m_i64W - 2*Win_cen, ar_cmmIn.m_i64W - 1] колонки\n");
	fprintf(pf, " for (; iCol < ar_cmmIn.m_i64W; iCol++)\n");
	fprintf(pf, " {\n");
	fprintf(pf, " iCmin = iCol - Win_cen;\n");
	fprintf(pf, " iCmax = min(ar_cmmIn.m_i64W - Win_cen - 1, iCol + Win_cen);\n");
	fprintf(pf, "\n");
	}
	fprintf(pf, " if ((d = (pRowIn[iCol] - pseudo_min)*kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0 при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!\n");
			if (0 == iFlagRows)
				fprintf(pf, " pbBrightnessRow[iCol] = 0;\n");
			else
				fprintf(pf, " u32ValueAdd = 0;\n");
	fprintf(pf, " else\n");
	fprintf(pf, " {\n");
	fprintf(pf, " if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)\n");
	if (0 == iFlagRows)
	{
		fprintf(pf, " {\n");
		fprintf(pf, " pbBrightnessRow[iCol] = u32ValueAdd = 255;\n");
		fprintf(pf, " pHistAdd = pHistAll + (255 * ar_cmmIn.m_i64W%s);\n", pcAdd_iCmin);
		fprintf(pf, " }\n");
		fprintf(pf, " else\n");
		fprintf(pf, " {\n");
		fprintf(pf, " pbBrightnessRow[iCol] = static_cast<uint8_t>(u32ValueAdd);\n");
		fprintf(pf, " pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W%s);\n",
			pcAdd_iCmin);
		fprintf(pf, " }\n");
	}
	else
	{
	fprintf(pf, " u32ValueAdd = 255;\n");
	fprintf(pf, " }\n");
	fprintf(pf, " if (u32ValueAdd != (u32ValueSub = pbBrightnessRow[iCol]))\n");
	fprintf(pf, " {\n");
	fprintf(pf, " pHistSub = pHist%s + (u32ValueSub * ar_cmmIn.m_i64W);\n", pcAll);
	fprintf(pf, " pHistAdd = pHist%s + (u32ValueAdd * ar_cmmIn.m_i64W);\n", pcAll);
	fprintf(pf, " pbBrightnessRow[iCol] = u32ValueAdd;\n");
	
	if ('\0' != pcCorrSum[0])
		fprintf(pf, " uint32_t u32ValueAddSub = u32ValueAdd + u32ValueSub;\n");

	fprintf(pf, "\n");
	}
	fprintf(pf, " // Добавляем яркость в Win_size гистограмм и сумм\n");
	if (1 != iFlagCols)
		fprintf(pf, " for (i = iCmin; i <= iCmax; i++)\n");
	else
		fprintf(pf, " for (i = 0; i < Win_size; i++)\n");
	if ('\0' == pcCorrSum[0])
		fprintf(pf, " puiSum%s[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++ ) << 1)); \n", pcCurr);
	else
		fprintf(pf, " puiSum%s[i] += u32ValueAddSub + ((u32ValueAdd * static_cast<uint32_t>(pHistAdd[i]++) - u32ValueSub * static_cast<uint32_t>(pHistSub[i]--)) << 1); \n", pcCurr);
		fprintf(pf, " }\n");
	if (0 != iFlagCols)
	{
		if (0 == iFlagRows)
		{
			fprintf(pf, " if ((Win_size - 1) == iRow)\n");
			fprintf(pf, " {\n");
			fprintf(pf, " pfRowOut[0] = static_cast<float>(puiSum%s[%s] * Size_obratn);\n",
				pcCurr, pcAdd_iCminOr0);
			fprintf(pf, " pfRowOut++;\n");
			fprintf(pf, " }\n");
		}
		else
		{
		fprintf(pf, " pfRowOut[0] = static_cast<float>(puiSum%s[%s] * Size_obratn);\n", pcCurr, pcAdd_iCminOr0);
		fprintf(pf, " pfRowOut++;\n");
		}
	}
	fprintf(pf, " }\n");
	return 0;
}
int iCodeGenRows_V8(FILE* pf, int Win_cen,  int iFlagRows)
{
	int Win_size = (Win_cen << 1) + 1;
	int Win_lin = Win_size * Win_size;
	fprintf(pf, "\n");
	if (0 == iFlagRows)
	{
		fprintf(pf, " // Первые [0, Win_size - 1] строки\n");
		fprintf(pf, " pbBrightnessRow = pbBrightness;\n");
		fprintf(pf, " float *pfRowOut = ar_cmmOut.pfGetRow(Win_cen) + Win_cen;\n");
		fprintf(pf, " for (iRow = 0; iRow < Win_size; iRow++, pbBrightnessRow += ar_cmmIn.m_i64W)\n");
	}
	else
	{
	fprintf(pf, " // Последующие строки [Win_size, ar_cmmIn.m_i64H[\n");
	fprintf(pf, " for (iRow = Win_size; iRow < ar_cmmIn.m_i64H; iRow++)\n");
	}
	fprintf(pf, " {\n");
	fprintf(pf, " // Обнуляем края строк.\n");
	fprintf(pf, " memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * Win_cen);\n");
	fprintf(pf, " memset(ar_cmmOut.pfGetRow(iRow) + (ar_cmmIn.m_i64W - Win_cen), 0, sizeof(float) * Win_cen); \n");
	fprintf(pf, "\n");
	fprintf(pf, " uint16_t *pRowIn = ar_cmmIn.pu16GetRow(iRow);\n");
	if (0 != iFlagRows)
	{
		fprintf(pf, " float *pfRowOut = ar_cmmOut.pfGetRow(iRow - Win_cen) + Win_cen;\n");
		fprintf(pf, " iPos = (iRow - Win_size) %% Win_size;\n");
		fprintf(pf, " pbBrightnessRow = pbBrightness + (iPos * ar_cmmIn.m_i64W);\n");
	}
	// Первые колонки
	if (0 != iCodeGenRowsCols_V8(pf,Win_cen, iFlagRows, 0))
		return 10;
	// Средние колонки
	if (0 != iCodeGenRowsCols_V8(pf, Win_cen, iFlagRows, 1))
		return 20;
	// Последние колонки
	if (0 != iCodeGenRowsCols_V8(pf, Win_cen, iFlagRows, 2))
		return 30;
	fprintf(pf, " }\n");
	return 0;
}
int iCodeGen_V8(const char* a_pcFilePath)
{
	FILE* pf = fopen(a_pcFilePath, "wt");
	if (nullptr == pf)
		return 10;
	fprintf(pf, "#define _CRT_SECURE_NO_WARNINGS\n");
	fprintf(pf, "\n");
	fprintf(pf, "#include <iostream>\n");
	fprintf(pf, "#include <conio.h>\n");
	fprintf(pf, "#include <string>\n");
	fprintf(pf, "#include <omp.h>\n");
	fprintf(pf, "\n");
	fprintf(pf, "#include \"MU16Data.h\"\n");
	fprintf(pf, "\n");
	fprintf(pf, "#include \"TextureFiltr_Mean_V8_CodeGen.h\"\n");
	for (int Win_cen = 1; Win_cen <= 10; Win_cen++)
	{
		fprintf(pf, "\n");
		int Win_size = (Win_cen << 1) + 1;
		int Win_lin = Win_size * Win_size;
		// Пролог
		fprintf(pf, "\n");
		fprintf(pf, "void TextureFiltr_Mean_V8_%d(MU16Data &ar_cmmIn, MFData &ar_cmmOut, double pseudo_min, double kfct)\n", Win_size);
		fprintf(pf, "{\n");
		fprintf(pf, " double Size_obratn = 1.0 / %d;\n", Win_lin);
		fprintf(pf, "\n");
		fprintf(pf, " // Кэш для гистограмм\n");
		fprintf(pf, " uint16_t *pHistAll = new uint16_t[256 * ar_cmmIn.m_i64W]; // [256][ar_cmmIn.m_i64W]\n");
		fprintf(pf, " memset(pHistAll, 0, sizeof(uint16_t) * 256 * ar_cmmIn.m_i64W);\n");
		fprintf(pf, " uint16_t *pHist, *pHistAdd, *pHistSub;\n");
		fprintf(pf, "\n");
		fprintf(pf, " // Кэш для сумм\n");
		fprintf(pf, " uint32_t *puiSum = new uint32_t[ar_cmmIn.m_i64W], *puiSumCurr;\n");
		fprintf(pf, " memset(puiSum, 0, sizeof(uint32_t) * ar_cmmIn.m_i64W);\n");
		fprintf(pf, "\n");
		fprintf(pf, " // Кэш для яркостей\n");
		fprintf(pf, " uint8_t *pbBrightness = new uint8_t[ar_cmmIn.m_i64W * %d];\n", Win_size);
		fprintf(pf, " uint8_t *pbBrightnessRow;\n");
		fprintf(pf, "\n");
		fprintf(pf, " int64_t iRow, iCol, iColMax = ar_cmmIn.m_i64W - %d;\n", Win_size);
		fprintf(pf, " ar_cmmOut.iCreate(ar_cmmIn.m_i64W, ar_cmmIn.m_i64H, 6);\n");
		fprintf(pf, "\n");
		fprintf(pf, " // Обнуляем края. Первые строки.\n");
		fprintf(pf, " for (iRow = 0; iRow < %d; iRow++)\n", Win_cen);
		fprintf(pf, " memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);\n");
		fprintf(pf, "\n");
		fprintf(pf, " int64_t i, iCmin, iCmax, iPos = 0;\n");
		fprintf(pf, " uint32_t u32ValueAdd, u32ValueSub;\n");
		fprintf(pf, " double d;\n");
		// Начальные строки
		if (0 != iCodeGenRows_V8(pf, Win_cen, 0))
		{
			fclose(pf);
			return 20;
		}
		// Последующие строки
		if (0 != iCodeGenRows_V8(pf, Win_cen, 1))
		{
			fclose(pf);
			return 30;
		}
		// Эпилог
		fprintf(pf, "\n");
		fprintf(pf, " // Обнуляем края. Последние строки.\n");
		fprintf(pf, " for (iRow = ar_cmmIn.m_i64H - %d; iRow < ar_cmmOut.m_i64H; iRow++)\n", Win_cen);
		fprintf(pf, " memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);\n");
		fprintf(pf, "\n");
		fprintf(pf, " if (nullptr != pHistAll)\n");
		fprintf(pf, " delete[] pHistAll;\n");
		fprintf(pf, " if (nullptr != puiSum)\n");
		fprintf(pf, " delete[] puiSum;\n");
		fprintf(pf, " if (nullptr != pbBrightness)\n");
		fprintf(pf, " delete[] pbBrightness;\n");
		fprintf(pf, "}\n");
	}
	fprintf(pf, "\n");
	fprintf(pf, "tdTextureFiltr_Mean_V8 g_afunTextureFiltr_Mean_V8[10] = \n");
	fprintf(pf, "{\n");
	fprintf(pf, " TextureFiltr_Mean_V8_3, // 0\n");
	fprintf(pf, " TextureFiltr_Mean_V8_5, // 1\n");
	fprintf(pf, " TextureFiltr_Mean_V8_7, // 2\n");
	fprintf(pf, " TextureFiltr_Mean_V8_9, // 3\n");
	fprintf(pf, " TextureFiltr_Mean_V8_11, // 4\n");
	fprintf(pf, " TextureFiltr_Mean_V8_13, // 5\n");
	fprintf(pf, " TextureFiltr_Mean_V8_15, // 6\n");
	fprintf(pf, " TextureFiltr_Mean_V8_17, // 7\n");
	fprintf(pf, " TextureFiltr_Mean_V8_19, // 8\n");
	fprintf(pf, " TextureFiltr_Mean_V8_21 // 9\n");
	fprintf(pf, "};\n");
	fclose(pf);
	return 0;
}

#include "TextureFiltr_Mean_V8_CodeGen.h"


int main()
{


	//iCodeGen_V8_sse4("D:/projects/SobolevTexFiltr/function.txt");

	//return 0;
	const char *pcFilePath = "D:/projects/SobolevTexFiltr/";
	const char *pcFileNameIn = "TextureFiltrIn.mu16";


	const char* pcFileNameStats = "stats.csv";

	std::string fileOut;
	fileOut = std::string(pcFilePath) + std::string(pcFileNameStats);

	//FILE* statFile; 
	//fopen_s(&statFile, fileOut.c_str(), "wb");

	//{
	//	MU16Data m;
	//	m.iCreate(5, 5, 1);
	//	int iNum = 0;
	//	for (int iRow = 0; iRow < 5; iRow++)
	//	{
	//		for (int iCol = 0; iCol < 5; iCol++)
	//		{
	//			m.m_ppRows[iRow][iCol] = iNum++;
	//		}
	//	}m
	//		.m_ppRows[4][4] = 255;
	//	m.iWrite("D:/projects/SobolevTexFiltr/test5x5.mu16");
	//}
	//return 0;
	
	
	std::string strFileIn, strFileOut;


	MU16Data m;
	MFData mOut;
	int Win_cen = 1;
	int Win_size = 2 * Win_cen + 1;
	double pseudo_min, kfct;

	strFileIn = pcFilePath;
	strFileIn += pcFileNameIn;
	m.iRead(strFileIn.c_str());

	// Найдем мин и мах
	uint16_t u16Min, u16Max;
	u16Min = u16Max = m.pu16GetRow(0)[0];
	for (int64_t iRow = 0; iRow < m.m_i64H; iRow++)
	{
		uint16_t *pRowIn = m.pu16GetRow(iRow);
		for (int64_t iCol = 0; iCol < m.m_i64W; iCol++)
		{
			if (u16Min > pRowIn[iCol])
				u16Min = pRowIn[iCol];
			else if (u16Max < pRowIn[iCol])
				u16Max = pRowIn[iCol];
		}
	}
	pseudo_min = u16Min;
	kfct = 255. / (u16Max - u16Min);

	//TextureFiltr_Mean(m, mOut, Win_cen, pseudo_min, kfct);
	std::string strDebug;
	strDebug = pcFilePath;
	strDebug += "Trace.txt";
	FILE* pf = fopen(strDebug.c_str(), "wb");
	if (nullptr == pf)
		return -1;

	std::string strTab, strValue, strPoint("."), strComma(",");
	strTab = pcFilePath;
	strTab += "TraceTab.csv";
	FILE* pfTab = fopen(strTab.c_str(), "a+t");
	if (nullptr == pfTab)
		return -2;
	double dStart, dEnd;
	
	//fprintf(pfTab, "Вариант функции;Потоков; 3x3; 5x5; 7x7; 9x9; 11x11;13x13;15x15;17x17;19x19;21x21;");
	fprintf(pfTab, "\n");
	std::string strFunName = "TextureFiltr_Mean_V8_3_sse";
	fprintf(pf, "\n%s\n", strFunName.c_str());
	for (int iThreads = 1; iThreads <= 1; iThreads <<= 1)
	{
		fprintf(pf, "%d\n", iThreads);
		fprintf(pfTab, "%s;%d;", strFunName.c_str(), iThreads);
			for (Win_cen = 1; Win_cen <= 10; Win_cen++)
			{
				int Win_size = 2 * Win_cen + 1;
				dStart = omp_get_wtime();
				TextureFiltr_Mean_V8_3_sse(m, mOut, Win_cen, pseudo_min, kfct);
				dEnd = omp_get_wtime();
				strFileOut = pcFilePath + std::string("Out") + std::to_string(Win_size) + "x" +
					std::to_string(Win_size) + "_src.mfd";
				mOut.iWrite(strFileOut.c_str());
				printf("%s - %lg\n", strFileOut.c_str(), dEnd - dStart);
				fprintf(pf, "%s - %lg\n", strFileOut.c_str(), dEnd - dStart);

				strValue = std::to_string(dEnd - dStart); // double -> string
				size_t pos = strValue.find(strPoint); // '.' -> ','
				while (pos != std::string::npos) {
					strValue.replace(pos, strComma.size(), strComma);
					pos = strValue.find(strPoint, pos);
				}
				fprintf(pfTab, "\t%s;", strValue.c_str());
				{// Сравнение результатов, полученных при применении исходного алгоритма и ускоренного
					MFData mSrc;
					std::string strFileTest = pcFilePath + std::string("Out") + std::to_string(Win_size) + "x" +
						std::to_string(Win_size) + "_src.mfd";
					if (0 != mSrc.iRead(strFileTest.c_str()))
					{
						printf("Не удалось прочитать файл '%s'\n", strFileTest.c_str());
						fprintf(pf, "Не удалось прочитать файл '%s'\n", strFileTest.c_str());
						fclose(pf);
						return -1;
					}
					std::stringstream ssCFL;
					int iRet = memcmp(mSrc.m_pData, mOut.m_pData, mSrc.m_i64LineSizeEl * mSrc.m_i64H * sizeof(float));
					if (0 != iRet)
					{
						printf("%s\n", ssCFL.str().c_str());
						fprintf(pf, "%s\n", ssCFL.str().c_str());
					}
					else
					{
						printf("EQ!\n");
					//	fprintf(pf, "EQ!\n");
					}
				}
			}
		fprintf(pfTab, "\n");
	}
	fclose(pf);
	fclose(pfTab);

	//strFileOut = pcFilePath + std::string("Out") + std::to_string(Win_size) + "x" + std::to_string(Win_size) + "_src.mfd";
	//mOut.iWrite(strFileOut.c_str());

	return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.

void TextureFiltr_Mean_OMP(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct, int a_iThreads)
{
	int i = (Win_cen << 1) + 1;
	int Win_lin = i * i;
	if (a_iThreads <= 0)
		a_iThreads = 1;
	uint8_t* pVarRowTh = new uint8_t[Win_lin * a_iThreads];
	BTexture* pbt = new BTexture[a_iThreads];
	for (i = 0; i < a_iThreads; i++)
		pbt[i].Create(pVarRowTh + (i * Win_lin), Win_cen);

	int64_t iRow, iCol, iRowMax = ar_cmmIn.m_i64H - Win_cen, iColMax = ar_cmmIn.m_i64W - Win_cen;
	ar_cmmOut.iCreate(ar_cmmIn.m_i64W, ar_cmmIn.m_i64H, 6);

	// Обнуляем края. Первые строки.
	for (iRow = 0; iRow < Win_cen; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);

	int ln, rw;
#pragma omp parallel private(ln, rw, iCol) num_threads(a_iThreads)
	{
		int iThreadNum = omp_get_thread_num();
		uint8_t* pVarRow = pVarRowTh + (iThreadNum * Win_lin);
		BTexture& bt = pbt[iThreadNum];
#pragma omp for
		for (iRow = Win_cen; iRow < iRowMax; iRow++)
		{
			float* pfRowOut = ar_cmmOut.pfGetRow(iRow);
			// Обнуляем края. Левый край.
			memset(pfRowOut, 0, sizeof(float) * Win_cen);

			for (iCol = Win_cen; iCol < iColMax; iCol++)
			{
				int i_num = 0;
				for (rw = -Win_cen; rw <= Win_cen; rw++)
				{
					uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow + rw);
					for (ln = -Win_cen; ln <= Win_cen; ln++)
					{
						double d = (pRowIn[iCol + ln] - pseudo_min) * kfct;
						if (d <= 0.)
							pVarRow[i_num] = 0;
						else
						{
							int iVal = static_cast<int>(d + 0.5);
							if (iVal > 255)
								pVarRow[i_num] = 255;
							else
								pVarRow[i_num] = static_cast<uint8_t>(iVal);
						}
						i_num++;
					}
				}
				pfRowOut[iCol] = static_cast<float>(bt.Calc_Mean());
			}

			// Обнуляем края. Правый край.
			memset(pfRowOut + iColMax, 0, sizeof(float) * (ar_cmmOut.m_i64LineSizeEl - iColMax));
		}


		// Обнуляем края. Последние строки.
		for (iRow = iRowMax; iRow < ar_cmmOut.m_i64H; iRow++)
			memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	}
	if (nullptr != pVarRowTh)
		delete[] pVarRowTh;
	if (nullptr != pbt)
		delete[] pbt;
}

void TextureFiltr_Mean_V2(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
	int i = (Win_cen << 1) + 1;
	int Win_lin = i * i;
	uint8_t* pVarRow = new uint8_t[Win_lin];
	uint16_t* pHist = new uint16_t[256];
	double Size_obratn = 1.0 / Win_lin;

	int64_t iRow, iCol, iRowMax = ar_cmmIn.m_i64H - Win_cen, iColMax = ar_cmmIn.m_i64W - Win_cen;
	ar_cmmOut.iCreate(ar_cmmIn.m_i64W, ar_cmmIn.m_i64H, 6);

	// Обнуляем края. Первые строки.
	for (iRow = 0; iRow < Win_cen; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);

	int ln, rw;
	for (iRow = Win_cen; iRow < iRowMax; iRow++)
	{
		float* pfRowOut = ar_cmmOut.pfGetRow(iRow);
		// Обнуляем края. Левый край.
		memset(pfRowOut, 0, sizeof(float) * Win_cen);

		for (iCol = Win_cen; iCol < iColMax; iCol++)
		{
			memset(pHist, 0, sizeof(uint16_t) * 256);
			int i_num = 0;
			for (rw = -Win_cen; rw <= Win_cen; rw++)
			{
				uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow + rw);
				for (ln = -Win_cen; ln <= Win_cen; ln++)
				{
					double d = (pRowIn[iCol + ln] - pseudo_min) * kfct;
					if (d <= 0.)
						pHist[pVarRow[i_num] = 0]++;
					else
					{
						int iVal = static_cast<int>(d + 0.5);
						if (iVal > 255)
							pHist[pVarRow[i_num] = 255]++;
						else
							pHist[pVarRow[i_num] = static_cast<uint8_t>(iVal)]++;

					}
					i_num++;
				}
			}
			uint32_t Sum = 0;
			for (int i = 0; i < Win_lin; i++)
			{
				Sum += pVarRow[i] * static_cast<uint32_t>(pHist[pVarRow[i]]);
			}
			pfRowOut[iCol] = static_cast<float>(Sum * Size_obratn);
		}

		// Обнуляем края. Правый край.
		memset(pfRowOut + iColMax, 0, sizeof(float) * (ar_cmmOut.m_i64LineSizeEl - iColMax));
	}

	// Обнуляем края. Последние строки.
	for (iRow = iRowMax; iRow < ar_cmmOut.m_i64H; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);

	if (nullptr != pVarRow)
		delete[] pVarRow;
	if (nullptr != pHist)
		delete[] pHist;

}

void TextureFiltr_Mean_V2_1(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
	int i = (Win_cen << 1) + 1;
	int Win_lin = i * i;
	uint8_t* pVarRow = new uint8_t[Win_lin];
	uint16_t* pHist = new uint16_t[256];
	double Size_obratn = 1.0 / Win_lin;

	int64_t iRow, iCol, iRowMax = ar_cmmIn.m_i64H - Win_cen, iColMax = ar_cmmIn.m_i64W - Win_cen;
	ar_cmmOut.iCreate(ar_cmmIn.m_i64W, ar_cmmIn.m_i64H, 6);

	// Обнуляем края. Первые строки.
	for (iRow = 0; iRow < Win_cen; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);

	int ln, rw;
	for (iRow = Win_cen; iRow < iRowMax; iRow++)
	{
		float* pfRowOut = ar_cmmOut.pfGetRow(iRow);
		// Обнуляем края. Левый край.
		memset(pfRowOut, 0, sizeof(float) * Win_cen);

		for (iCol = Win_cen; iCol < iColMax; iCol++)
		{
			memset(pHist, 0, sizeof(uint16_t) * 256);
			int i_num = 0;
			for (rw = -Win_cen; rw <= Win_cen; rw++)
			{
				uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow + rw);
				for (ln = -Win_cen; ln <= Win_cen; ln++)
				{
					double d = (pRowIn[iCol + ln] - pseudo_min) * kfct;
					if (d <= 0.)
						pHist[pVarRow[i_num] = 0]++;
					else
					{
						int iVal = static_cast<int>(d + 0.5);
						if (iVal > 255)
							pHist[pVarRow[i_num] = 255]++;
						else
							pHist[pVarRow[i_num] = static_cast<uint8_t>(iVal)]++;

					}
					i_num++;
				}
			}
			uint32_t Sum = 0;
			for (uint32_t i = 0; i < 256; i++)
			{
				uint32_t iH = pHist[i];
				Sum += i * iH * iH;
			}
			pfRowOut[iCol] = static_cast<float>(Sum * Size_obratn);
		}

		// Обнуляем края. Правый край.
		memset(pfRowOut + iColMax, 0, sizeof(float) * (ar_cmmOut.m_i64LineSizeEl - iColMax));
	}

	// Обнуляем края. Последние строки.
	for (iRow = iRowMax; iRow < ar_cmmOut.m_i64H; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);

	if (nullptr != pVarRow)
		delete[] pVarRow;
	if (nullptr != pHist)
		delete[] pHist;

}

void TextureFiltr_Mean_V3(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
	int i = (Win_cen << 1) + 1;
	int Win_lin = i * i;
	uint8_t* pVarRow = new uint8_t[Win_lin];
	uint16_t* pHist = new uint16_t[256];
	double Size_obratn = 1.0 / Win_lin;

	int64_t iRow, iCol, iRowMax = ar_cmmIn.m_i64H - Win_cen, iColMax = ar_cmmIn.m_i64W - Win_cen;
	ar_cmmOut.iCreate(ar_cmmIn.m_i64W, ar_cmmIn.m_i64H, 6);

	// Обнуляем края. Первые строки.
	for (iRow = 0; iRow < Win_cen; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);

	int ln, rw;
	for (iRow = Win_cen; iRow < iRowMax; iRow++)
	{
		float* pfRowOut = ar_cmmOut.pfGetRow(iRow);
		// Обнуляем края. Левый край.
		memset(pfRowOut, 0, sizeof(float) * Win_cen);

		for (iCol = Win_cen; iCol < iColMax; iCol++)
		{
			memset(pHist, 0, sizeof(uint16_t) * 256);
			int i_num = 0;
			for (rw = -Win_cen; rw <= Win_cen; rw++)
			{
				uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow + rw);
				for (ln = -Win_cen; ln <= Win_cen; ln++)
				{
					double d = (pRowIn[iCol + ln] - pseudo_min) * kfct;
					if (d <= 0.)
						pHist[pVarRow[i_num] = 0]++;
					else
					{
						int iVal = static_cast<int>(d + 0.5);
						if (iVal > 255)
							pHist[pVarRow[i_num] = 255]++;
						else
							pHist[pVarRow[i_num] = static_cast<uint8_t>(iVal)]++;

					}
					i_num++;
				}
			}
			uint32_t Sum = 0;
			for (int i = 0; i < Win_lin; i++)
			{
				uint32_t iValue = static_cast<uint32_t>(pVarRow[i]);
				uint32_t iH = static_cast<uint32_t>(pHist[iValue]);
				Sum += iValue * iH * iH;
				pHist[iValue] = 0;
			}
			pfRowOut[iCol] = static_cast<float>(Sum * Size_obratn);
		}

		// Обнуляем края. Правый край.
		memset(pfRowOut + iColMax, 0, sizeof(float) * (ar_cmmOut.m_i64LineSizeEl - iColMax));
	}

	// Обнуляем края. Последние строки.
	for (iRow = iRowMax; iRow < ar_cmmOut.m_i64H; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);

	if (nullptr != pVarRow)
		delete[] pVarRow;
	if (nullptr != pHist)
		delete[] pHist;

}

void TextureFiltr_Mean_V4(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
	int Win_size = (Win_cen << 1) + 1;
	int Win_lin = Win_size * Win_size;
	uint8_t* pVarRow = new uint8_t[Win_lin];
	uint16_t* pHist = new uint16_t[256];
	double Size_obratn = 1.0 / Win_lin;
	int64_t iRow, iCol, iRowMax = ar_cmmIn.m_i64H - Win_cen, iColMax = ar_cmmIn.m_i64W - Win_cen;
	ar_cmmOut.iCreate(ar_cmmIn.m_i64W, ar_cmmIn.m_i64H, 6);
	// Обнуляем края. Первые строки.
	for (iRow = 0; iRow < Win_cen; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	int ln, rw;
	for (iRow = Win_cen; iRow < iRowMax; iRow++)
	{
		float* pfRowOut = ar_cmmOut.pfGetRow(iRow);
		// Обнуляем края. Левый край.
		memset(pfRowOut, 0, sizeof(float) * Win_cen);
		// Первая точка в строке
		int iPos = 0;
		memset(pHist, 0, sizeof(uint16_t) * 256);
		iCol = Win_cen;
		{
			for (rw = -Win_cen; rw <= Win_cen; rw++)
			{
				uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow + rw);
				for (ln = -Win_cen; ln <= Win_cen; ln++)
				{
					double d = (pRowIn[iCol + ln] - pseudo_min) * kfct;
					if (d <= 0.)
						pHist[pVarRow[(ln + Win_cen) * Win_size + (rw + Win_cen)] = 0]++;
					else
					{
						int iVal = static_cast<int>(d + 0.5);
						if (iVal > 255)
							pHist[pVarRow[(ln + Win_cen) * Win_size + (rw + Win_cen)] = 255]++;
						else
							pHist[pVarRow[(ln + Win_cen) * Win_size + (rw + Win_cen)] =
							static_cast<uint8_t>(iVal)]++;
					}
				}
			}
			uint32_t Sum = 0;
			for (int i = 0; i < Win_lin; i++)
			{
				uint32_t iValue = static_cast<uint32_t>(pVarRow[i]);
				Sum += iValue * static_cast<uint32_t>(pHist[iValue]);
			}pfRowOut[iCol] = static_cast<float>(Sum * Size_obratn);
		}
		for (++iCol; iCol < iColMax; iCol++)
		{
			uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow - Win_cen) + iCol + Win_cen;
			for (rw = 0; rw < Win_size; rw++, pRowIn += ar_cmmIn.m_i64LineSizeEl)
			{
				// Вычитаем из гистограммы столбец iCol - Win_cen - 1
				pHist[pVarRow[iPos + rw]]--;
				// Добавим в гистограмму столбец iCol + Win_cen
				double d = (pRowIn[0] - pseudo_min) * kfct;
				if (d <= 0.)
					pHist[pVarRow[iPos + rw] = 0]++;
				else
				{
					int iVal = static_cast<int>(d + 0.5);
					if (iVal > 255)
						pHist[pVarRow[iPos + rw] = 255]++;
					else
					{
						pHist[pVarRow[iPos + rw] = static_cast<uint8_t>(iVal)]++;
					}
				}
			}
			iPos += Win_size;
			if (iPos >= Win_lin)
				iPos = 0;
			uint32_t Sum = 0;
			for (int i = 0; i < Win_lin; i++)
			{
				uint32_t iValue = static_cast<uint32_t>(pVarRow[i]);
				Sum += iValue * static_cast<uint32_t>(pHist[iValue]);
			}
			pfRowOut[iCol] = static_cast<float>(Sum * Size_obratn);
		}
		// Обнуляем края. Правый край.
		memset(pfRowOut + iColMax, 0, sizeof(float)* (ar_cmmOut.m_i64LineSizeEl - iColMax));
	}
	// Обнуляем края. Последние строки.
	for (iRow = iRowMax; iRow < ar_cmmOut.m_i64H; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	if (nullptr != pVarRow)
		delete[] pVarRow;
	if (nullptr != pHist)
		delete[] pHist;
}


void TextureFiltr_Mean_V5(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
	int Win_size = (Win_cen << 1) + 1;
	int Win_lin = Win_size * Win_size;
	uint8_t* pVarRow = new uint8_t[Win_lin];
	uint16_t* pHist = new uint16_t[256];
	double Size_obratn = 1.0 / Win_lin;
	int64_t iRow, iCol, iRowMax = ar_cmmIn.m_i64H - Win_cen, iColMax = ar_cmmIn.m_i64W - Win_cen;
	ar_cmmOut.iCreate(ar_cmmIn.m_i64W, ar_cmmIn.m_i64H, 6);
	// Обнуляем края. Первые строки.
	for (iRow = 0; iRow < Win_cen; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	int ln, rw;
	for (iRow = Win_cen; iRow < iRowMax; iRow++)
	{
		float* pfRowOut = ar_cmmOut.pfGetRow(iRow);
		// Обнуляем края. Левый край.
		memset(pfRowOut, 0, sizeof(float) * Win_cen);
		// Первая точка в строке
		int iPos = 0;
		uint32_t Sum = 0;
		memset(pHist, 0, sizeof(uint16_t) * 256);
		iCol = Win_cen;
		{
			for (rw = -Win_cen; rw <= Win_cen; rw++)
			{
				uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow + rw);
				for (ln = -Win_cen; ln <= Win_cen; ln++)
				{
					double d = (pRowIn[iCol + ln] - pseudo_min) * kfct;
					if (d <= 0.)
						pHist[pVarRow[(ln + Win_cen) * Win_size + (rw + Win_cen)] = 0]++;
					else
					{
						int iVal = static_cast<int>(d + 0.5);
						if (iVal > 255)
							pHist[pVarRow[(ln + Win_cen) * Win_size + (rw + Win_cen)] = 255]++;
						else
							pHist[pVarRow[(ln + Win_cen) * Win_size + (rw + Win_cen)] =
							static_cast<uint8_t>(iVal)]++;
					}
				}
			}

			for (int i = 0; i < Win_lin; i++)
			{
				uint32_t iValue = static_cast<uint32_t>(pVarRow[i]);
				Sum += iValue * static_cast<uint32_t>(pHist[iValue]);
			}pfRowOut[iCol] = static_cast<float>(Sum * Size_obratn);
		}
		for (++iCol; iCol < iColMax; iCol++)
		{
			uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow - Win_cen) + iCol + Win_cen;
			for (rw = 0; rw < Win_size; rw++, pRowIn += ar_cmmIn.m_i64LineSizeEl)
			{
				// Вычитаем из гистограммы столбец iCol - Win_cen - 1
				uint32_t iValue = pVarRow[iPos + rw];
				uint32_t iH = pHist[iValue]--;
				Sum += iValue * (1 - (iH << 1));
				// Добавим в гистограмму столбец iCol + Win_cen
				double d = (pRowIn[0] - pseudo_min) * kfct;
				if (d <= 0.)
					pVarRow[iPos + rw] = 0;
				else
				{
					int iVal = static_cast<int>(d + 0.5);
					if (iVal > 255)
					{
						iH = pHist[pVarRow[iPos + rw] = 255]++;
						Sum += 255 * (1 + (iH << 1));
					}else
					{
						iH = pHist[pVarRow[iPos + rw] = static_cast<uint8_t>(iVal)]++;
						Sum += iVal * (1 + (iH << 1));
					}
				}
			}
			iPos += Win_size;
			if (iPos >= Win_lin)
				iPos = 0;
			uint32_t Sum = 0;
			for (int i = 0; i < Win_lin; i++)
			{
				uint32_t iValue = static_cast<uint32_t>(pVarRow[i]);
				Sum += iValue * static_cast<uint32_t>(pHist[iValue]);
			}
			pfRowOut[iCol] = static_cast<float>(Sum * Size_obratn);
		}
		// Обнуляем края. Правый край.
		memset(pfRowOut + iColMax, 0, sizeof(float) * (ar_cmmOut.m_i64LineSizeEl - iColMax));
	}
	// Обнуляем края. Последние строки.
	for (iRow = iRowMax; iRow < ar_cmmOut.m_i64H; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	if (nullptr != pVarRow)
		delete[] pVarRow;
	if (nullptr != pHist)
		delete[] pHist;

}

void TextureFiltr_Mean_V6(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
	int Win_size = (Win_cen << 1) + 1;
	int Win_lin = Win_size * Win_size;
	uint8_t* pVarRow = new uint8_t[Win_lin];
	uint16_t* pHist;
	double Size_obratn = 1.0 / Win_lin;
	// Кэш для гистограмм
	uint16_t* pHistAll = new uint16_t[256 * ar_cmmIn.m_i64W];
	size_t uiHistSize = 256 * sizeof(uint16_t);
	// Кэш для сумм
	uint32_t* puiSum = new uint32_t[ar_cmmIn.m_i64W];
	// Кэш для яркостей
	uint8_t* pbBrightness = new uint8_t[ar_cmmIn.m_i64W * (Win_size + 1)];
	uint8_t* pbBrightnessRow;
	int64_t iRow, iCol, iRowMax = ar_cmmIn.m_i64H - Win_cen, iColMax = ar_cmmIn.m_i64W - Win_cen;
	ar_cmmOut.iCreate(ar_cmmIn.m_i64W, ar_cmmIn.m_i64H, 6);
	// Обнуляем края. Первые строки.
	for (iRow = 0; iRow < Win_cen; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	int ln, rw, iPos = 0;
	uint32_t iValue, iH, Sum = 0;
	{// Расчет кэшей, первая строка
		iRow = Win_cen;
		// Первая точка в строке
		iCol = Win_cen;
		pHist = pHistAll + 256 * iCol;
		memset(pHist, 0, uiHistSize);
		float* pfRowOut = ar_cmmOut.pfGetRow(iRow);
		// Обнуляем края. Левый край.
		memset(pfRowOut, 0, sizeof(float) * Win_cen);
		{
			pbBrightnessRow = pbBrightness + iCol;
			for (rw = -Win_cen; rw <= Win_cen; rw++, pbBrightnessRow += ar_cmmIn.m_i64W)
			{
				uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow + rw);
				for (ln = -Win_cen; ln <= Win_cen; ln++)
				{
					double d = (pRowIn[iCol + ln] - pseudo_min) * kfct;
					if (d < 0.5) // Так как при яркости 0 вклад в сумму будет 0 при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но pVarRow нужно обнулить!
						pbBrightnessRow[ln] = pVarRow[(ln + Win_cen) * Win_size + (rw + Win_cen)] = 0;
					else
					{
						if ((iValue = static_cast<uint32_t>(d + 0.5)) > 255)
						{
							pbBrightnessRow[ln] = pVarRow[(ln + Win_cen) * Win_size + (rw + Win_cen)] = 255;
							pHist[255]++;
						}
						else
						{
						pbBrightnessRow[ln] = pVarRow[(ln + Win_cen) * Win_size + (rw + Win_cen)] =
						static_cast<uint8_t>(iValue);
						pHist[iValue]++;
						}
					}
				}
			}for (int i = 0; i < Win_lin; i++)
			{
				iValue = static_cast<uint32_t>(pVarRow[i]);
				Sum += iValue * static_cast<uint32_t>(pHist[iValue]);
			}
			pfRowOut[iCol] = static_cast<float>(Sum * Size_obratn);
			puiSum[iCol] = Sum;
		}
		// Следующие точки в строке
		for (++iCol; iCol < iColMax; iCol++)
		{
			// Копируем гистограммы
			pHist = pHistAll + 256 * iCol;
			memcpy(pHist, pHist - 256, uiHistSize);
			uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow - Win_cen) + iCol + Win_cen;
			pbBrightnessRow = pbBrightness + iCol + Win_cen;
			for (rw = 0; rw < Win_size; rw++, pRowIn += ar_cmmIn.m_i64LineSizeEl, pbBrightnessRow +=
				ar_cmmIn.m_i64W)
			{
				// Вычитаем из гистограммы столбец iCol - Win_cen - 1
				iH = pHist[iValue = pVarRow[iPos + rw]]--;
				Sum += iValue * (1 - (iH << 1));
				// Прибавляем в гистограмму столбец iCol + Win_cen
				double d = (pRowIn[0] - pseudo_min) * kfct;
				if (d < 0.5) // Так как при яркости 0 вклад в сумму будет 0 при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но pVarRow нужно обнулить!
					pbBrightnessRow[0] = pVarRow[iPos + rw] = 0;
				else
				{
					if ((iValue = static_cast<uint32_t>(d + 0.5)) > 255)
					{
						pbBrightnessRow[0] = pVarRow[iPos + rw] = 255;
						iH = pHist[255]++;
						Sum += 255 * (1 + (iH << 1));
					}else
					{
					pbBrightnessRow[0] = pVarRow[iPos + rw] = static_cast<uint8_t>(iValue);
					iH = pHist[iValue]++;
					Sum += iValue * (1 + (iH << 1));
					}
				}
			}
			iPos += Win_size;
			if (iPos >= Win_lin)
				iPos = 0;
			pfRowOut[iCol] = static_cast<float>(Sum * Size_obratn);
			puiSum[iCol] = Sum;
		}
		// Обнуляем края. Правый край.
		memset(pfRowOut + iColMax, 0, sizeof(float)* (ar_cmmOut.m_i64LineSizeEl - iColMax));
	}
	// Последующие строки
	int iBrRowSub = 0; // Из какой строки яркостей будем вычитать [0, Win_size]
	int iBrRowAdd = Win_size; // В какую строку яркостей будем заносить данные [0, Win_size]
	for (++iRow; iRow < iRowMax; iRow++)
	{
		float* pfRowOut = ar_cmmOut.pfGetRow(iRow);
		// Обнуляем края. Левый край.
		memset(pfRowOut, 0, sizeof(float) * Win_cen);
		uint8_t* pbBrightnessRowSub = pbBrightness + iBrRowSub * ar_cmmIn.m_i64W;
		uint8_t* pbBrightnessRowAdd = pbBrightness + iBrRowAdd * ar_cmmIn.m_i64W;
		uint16_t* pRowInAdd = ar_cmmIn.pu16GetRow(iRow + Win_cen);
		// Первая точка в строке
		iCol = Win_cen;
		pHist = pHistAll + 256 * iCol;
		Sum = puiSum[iCol];
		{
			for (ln = 0; ln < Win_size; ln++)
			{
				// Вычитаем из гистограммы строку iRow - Win_cen - 1, колонки [iCol-Win_cen, iCol+Win_cen]
				iH = pHist[iValue = pbBrightnessRowSub[ln]]--;
				Sum += iValue * (1 - (iH << 1));
				// Прибавляем в гистограмму строку iRow + Win_cen, колонки [iCol-Win_cen, iCol+Win_cen]
				double d = (pRowInAdd[ln] - pseudo_min) * kfct;
				if (d < 0.5) // Так как при яркости 0 вклад в сумму будет 0 при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но pVarRow нужно обнулить!
					pbBrightnessRowAdd[ln] = 0;
				else
				{
					if ((iValue = static_cast<uint32_t>(d + 0.5)) > 255)
					{
						pbBrightnessRowAdd[ln] = 255;
						iH = pHist[255]++;
						Sum += 255 * (1 + (iH << 1));
					}
					else
					{
					pbBrightnessRowAdd[ln] = static_cast<uint8_t>(iValue);
					iH = pHist[iValue]++;
					Sum += iValue * (1 + (iH << 1));
					}
				}
			}
			pfRowOut[iCol] = static_cast<float>(Sum * Size_obratn);
			puiSum[iCol] = Sum;
		}
		// Следующие точки в строке
		pbBrightnessRowSub++;
		pbBrightnessRowAdd++;
		pRowInAdd++;
		pHist += 256;
		for (++iCol; iCol < iColMax; iCol++, pbBrightnessRowSub++, pbBrightnessRowAdd++, pRowInAdd++, pHist += 256)
		{
			Sum = puiSum[iCol];
			for (ln = 0; ln < Win_size - 1; ln++)
			{// Все данные есть в строке яркостей
			// Вычитаем из гистограммы строку iRow - Win_cen - 1, колонки [iCol-Win_cen, iCol+Win_cen]
				iH = pHist[iValue = pbBrightnessRowSub[ln]]--;
				Sum += iValue * (1 - (iH << 1));
				// Прибавляем в гистограмму строку iRow + Win_cen, колонки [iCol-Win_cen, iCol+Win_cen]
				iH = pHist[iValue = pbBrightnessRowAdd[ln]]++;
				Sum += iValue * (1 + (iH << 1));
			}
			{
				// Для добавляемой точки нужно рассчитать яркость
				// Вычитаем из гистограммы строку iRow - Win_cen - 1, колонки [iCol-Win_cen, iCol+Win_cen]
				iH = pHist[iValue = pbBrightnessRowSub[ln]]--;
				Sum += iValue * (1 - (iH << 1));
				// Прибавляем в гистограмму строку iRow + Win_cen, колонки [iCol-Win_cen, iCol+Win_cen]
				double d = (pRowInAdd[ln] - pseudo_min) * kfct;
				if (d < 0.5) // Так как при яркости 0 вклад в сумму будет 0 при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но pVarRow нужно обнулить!
					pbBrightnessRowAdd[ln] = 0;
				else
				{
					if ((iValue = static_cast<uint32_t>(d + 0.5)) > 255)
					{
						pbBrightnessRowAdd[ln] = 255;
						iH = pHist[255]++;
						Sum += 255 * (1 + (iH << 1));
					}
					else
					{
						pbBrightnessRowAdd[ln] = static_cast<uint8_t>(iValue);
						iH = pHist[iValue]++;
						Sum += iValue * (1 + (iH << 1));
					}
				}
			}
			pfRowOut[iCol] = static_cast<float>(Sum * Size_obratn);
			puiSum[iCol] = Sum;
		}
	// Обнуляем края. Правый край.
	memset(pfRowOut + iColMax, 0, sizeof(float)* (ar_cmmOut.m_i64LineSizeEl - iColMax));
	++iBrRowSub;
	if (iBrRowSub > Win_size)
		iBrRowSub = 0;
	++iBrRowAdd;
	if (iBrRowAdd > Win_size)
		iBrRowAdd = 0;
	}
// Обнуляем края. Последние строки.
	for (iRow = iRowMax; iRow < ar_cmmOut.m_i64H; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	if (nullptr != pVarRow)
		delete[] pVarRow;
	if (nullptr != pHistAll)
		delete[] pHistAll;
	if (nullptr != puiSum)
		delete[] puiSum;
	if (nullptr != pbBrightness)
		delete[] pbBrightness;
}

void TextureFiltr_Mean_V7(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
	int Win_size = (Win_cen << 1) + 1;
	int Win_lin = Win_size * Win_size;
	double Size_obratn = 1.0 / Win_lin;
	// Кэш для гистограмм
	uint16_t* pHistAll = new uint16_t[256 * ar_cmmIn.m_i64W]; // [256][ar_cmmIn.m_i64W]
	memset(pHistAll, 0, sizeof(uint16_t) * 256 * ar_cmmIn.m_i64W);
	uint16_t* pHist, * pHistAdd, * pHistSub;
	// Кэш для сумм
	uint32_t* puiSum = new uint32_t[ar_cmmIn.m_i64W], * puiSumCurr;
	memset(puiSum, 0, sizeof(uint32_t) * ar_cmmIn.m_i64W);
	// Кэш для яркостей
	uint8_t* pbBrightness = new uint8_t[ar_cmmIn.m_i64W * Win_size];
	uint8_t* pbBrightnessRow;
	int64_t iRow, iCol, iColMax = ar_cmmIn.m_i64W - Win_size;
	ar_cmmOut.iCreate(ar_cmmIn.m_i64W, ar_cmmIn.m_i64H, 6);
	// Обнуляем края. Первые строки.
	for (iRow = 0; iRow < Win_cen; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	int64_t i, iCmin, iCmax, iPos = 0;
	uint32_t u32ValueAdd, u32ValueSub;
	double d;
	// Первые [0, Win_size - 1] строки
	pbBrightnessRow = pbBrightness;
	float* pfRowOut = ar_cmmOut.pfGetRow(Win_cen) + Win_cen;
	for (iRow = 0; iRow < Win_size; iRow++, pbBrightnessRow += ar_cmmIn.m_i64W)
	{
		// Обнуляем края строк.
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * Win_cen);
		memset(ar_cmmOut.pfGetRow(iRow) + (ar_cmmIn.m_i64W - Win_cen), 0, sizeof(float) * Win_cen);
		uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow);
		// Первые [0, 2*Win_cen[ колонки
		for (iCol = 0; iCol < (Win_cen << 1); iCol++)
		{
			iCmin = std::max((long long)Win_cen, iCol - Win_cen);
			iCmax = iCol + Win_cen;
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				pbBrightnessRow[iCol] = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
				{
					pbBrightnessRow[iCol] = u32ValueAdd = 255;
					pHistAdd = pHistAll + (255 * ar_cmmIn.m_i64W);
				}else
				{
				pbBrightnessRow[iCol] = static_cast<uint8_t>(u32ValueAdd);
				pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W);
				}// Добавляем яркость в Win_size гистограмм и сумм
					for (i = iCmin; i <= iCmax; i++)
						puiSum[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1));
			}
		}
		// Средние [2*Win_cen, ar_cmmIn.m_i64W - Win_size] колонки
		puiSumCurr = puiSum + Win_cen;
		for (; iCol <= iColMax; iCol++, puiSumCurr++)
		{
			iCmin = iCol - Win_cen;
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				pbBrightnessRow[iCol] = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
				{
					pbBrightnessRow[iCol] = u32ValueAdd = 255;
					pHistAdd = pHistAll + (255 * ar_cmmIn.m_i64W + iCmin);
				}else
				{
				pbBrightnessRow[iCol] = static_cast<uint8_t>(u32ValueAdd);
				pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W + iCmin);
				}// Добавляем яркость в Win_size гистограмм и сумм
					for (i = 0; i < Win_size; i++)
						puiSumCurr[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1));
			}
			if((Win_size - 1) == iRow)
			{
				pfRowOut[0] = static_cast<float>(puiSumCurr[0] * Size_obratn);
				pfRowOut++;
			}
		}
		// Последние [ar_cmmIn.m_i64W - 2*Win_cen, ar_cmmIn.m_i64W - 1] колонки
		for (; iCol < ar_cmmIn.m_i64W; iCol++)
		{
			iCmin = iCol - Win_cen;
			iCmax = std::min(ar_cmmIn.m_i64W - Win_cen - 1, iCol + Win_cen);
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				pbBrightnessRow[iCol] = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
				{
					pbBrightnessRow[iCol] = u32ValueAdd = 255;
					pHistAdd = pHistAll + (255 * ar_cmmIn.m_i64W);
				}
				else
				{
					pbBrightnessRow[iCol] = static_cast<uint8_t>(u32ValueAdd);
					pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W);
				}// Добавляем яркость в Win_size гистограмм и сумм
					for (i = iCmin; i <= iCmax; i++)
						puiSum[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1));
			}
			if((Win_size - 1) == iRow)
			{
				pfRowOut[0] = static_cast<float>(puiSum[iCmin] * Size_obratn);
				pfRowOut++;
			}
		}
	}
	// Последующие строки [Win_size, ar_cmmIn.m_i64H[
	for (iRow = Win_size; iRow < ar_cmmIn.m_i64H; iRow++)
	{
		// Обнуляем края строк.
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * Win_cen);
		memset(ar_cmmOut.pfGetRow(iRow) + (ar_cmmIn.m_i64W - Win_cen), 0, sizeof(float) * Win_cen);
		uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow);
		float* pfRowOut = ar_cmmOut.pfGetRow(iRow - Win_cen) + Win_cen;
		iPos = (iRow - Win_size) % Win_size;
		pbBrightnessRow = pbBrightness + (iPos * ar_cmmIn.m_i64W);
		// Первые [0, 2*Win_cen[ колонки
		for (iCol = 0; iCol < (Win_cen << 1); iCol++)
		{
			iCmin = std::max((long long)Win_cen, iCol - Win_cen);
			iCmax = iCol + Win_cen;
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				u32ValueAdd = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
					u32ValueAdd = 255;
			}
			if(u32ValueAdd != (u32ValueSub = pbBrightnessRow[iCol]))
			{
				pHistSub = pHistAll + (u32ValueSub * ar_cmmIn.m_i64W);
				pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W);
				pbBrightnessRow[iCol] = u32ValueAdd;
				// Добавляем яркость в Win_size гистограмм и сумм
				for (i = iCmin; i <= iCmax; i++)
					puiSum[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1)) -
					u32ValueSub * ((static_cast<uint32_t>(pHistSub[i]--) << 1) - 1);
			}
		}
		// Средние [2*Win_cen, ar_cmmIn.m_i64W - Win_size] колонки
		puiSumCurr = puiSum + Win_cen;
		pHist = pHistAll + (iCol - Win_cen);
		for (; iCol <= iColMax; iCol++, puiSumCurr++, pHist++)
		{
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
			//	при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				u32ValueAdd = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
					u32ValueAdd = 255;
			}
			if(u32ValueAdd != (u32ValueSub = pbBrightnessRow[iCol]))
			{
				pHistSub = pHist + (u32ValueSub * ar_cmmIn.m_i64W);
				pHistAdd = pHist + (u32ValueAdd * ar_cmmIn.m_i64W);
				pbBrightnessRow[iCol] = u32ValueAdd;
				// Добавляем яркость в Win_size гистограмм и сумм
				for (i = 0; i < Win_size; i++)
					puiSumCurr[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1)) -
					u32ValueSub * ((static_cast<uint32_t>(pHistSub[i]--) << 1) - 1);
			}
			pfRowOut[0] = static_cast<float>(puiSumCurr[0] * Size_obratn);
			pfRowOut++;
		}
		// Последние [ar_cmmIn.m_i64W - 2*Win_cen, ar_cmmIn.m_i64W - 1] колонки
		for (; iCol < ar_cmmIn.m_i64W; iCol++)
		{
			iCmin = iCol - Win_cen;
			iCmax = std::min(ar_cmmIn.m_i64W - Win_cen - 1, iCol + Win_cen);
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				u32ValueAdd = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
					u32ValueAdd = 255;
			}
			if(u32ValueAdd != (u32ValueSub = pbBrightnessRow[iCol]))
			{
				pHistSub = pHistAll + (u32ValueSub * ar_cmmIn.m_i64W);
				pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W);
				pbBrightnessRow[iCol] = u32ValueAdd;
				// Добавляем яркость в Win_size гистограмм и сумм
				for (i = iCmin; i <= iCmax; i++)
					puiSum[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1)) -
					u32ValueSub * ((static_cast<uint32_t>(pHistSub[i]--) << 1) - 1);
			}
			pfRowOut[0] = static_cast<float>(puiSum[iCmin] * Size_obratn);
			pfRowOut++;
		}
	}
	// Обнуляем края. Последние строки.
	for (iRow = ar_cmmIn.m_i64H - Win_cen; iRow < ar_cmmOut.m_i64H; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	if (nullptr != pHistAll)
		delete[] pHistAll;
	if (nullptr != puiSum)
		delete[] puiSum;
	if (nullptr != pbBrightness)
		delete[] pbBrightness;
}


void TextureFiltr_Mean_V8(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
	int Win_size = (Win_cen << 1) + 1;
	int Win_lin = Win_size * Win_size;
	double Size_obratn = 1.0 / Win_lin;
	// Кэш для гистограмм
	uint16_t* pHistAll = new uint16_t[256 * ar_cmmIn.m_i64W]; // [256][ar_cmmIn.m_i64W]
	memset(pHistAll, 0, sizeof(uint16_t) * 256 * ar_cmmIn.m_i64W);
	uint16_t* pHist, * pHistAdd, * pHistSub;
	// Кэш для сумм
	uint32_t* puiSum = new uint32_t[ar_cmmIn.m_i64W], * puiSumCurr;
	memset(puiSum, 0, sizeof(uint32_t) * ar_cmmIn.m_i64W);
	// Кэш для яркостей
	uint8_t* pbBrightness = new uint8_t[ar_cmmIn.m_i64W * Win_size];
	uint8_t* pbBrightnessRow;
	int64_t iRow, iCol, iColMax = ar_cmmIn.m_i64W - Win_size;
	ar_cmmOut.iCreate(ar_cmmIn.m_i64W, ar_cmmIn.m_i64H, 6);
	// Обнуляем края. Первые строки.
	for (iRow = 0; iRow < Win_cen; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	int64_t i, iCmin, iCmax, iPos = 0;
	uint32_t u32ValueAdd, u32ValueSub;
	double d;
	// Первые [0, Win_size - 1] строки
	pbBrightnessRow = pbBrightness;
	float* pfRowOut = ar_cmmOut.pfGetRow(Win_cen) + Win_cen;
	for (iRow = 0; iRow < Win_size; iRow++, pbBrightnessRow += ar_cmmIn.m_i64W)
	{
		// Обнуляем края строк.
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * Win_cen);
		memset(ar_cmmOut.pfGetRow(iRow) + (ar_cmmIn.m_i64W - Win_cen), 0, sizeof(float) * Win_cen);
		uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow);
		// Первые [0, 2*Win_cen[ колонки
		for (iCol = 0; iCol < (Win_cen << 1); iCol++)
		{
			iCmin = std::max((long long)Win_cen, iCol - Win_cen);
			iCmax = iCol + Win_cen;
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				pbBrightnessRow[iCol] = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
				{
					pbBrightnessRow[iCol] = u32ValueAdd = 255;
					pHistAdd = pHistAll + (255 * ar_cmmIn.m_i64W);
				}
				else
				{
					pbBrightnessRow[iCol] = static_cast<uint8_t>(u32ValueAdd);
					pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W);
				}// Добавляем яркость в Win_size гистограмм и сумм
				for (i = iCmin; i <= iCmax; i++)
					puiSum[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1));
			}
		}
		// Средние [2*Win_cen, ar_cmmIn.m_i64W - Win_size] колонки
		puiSumCurr = puiSum + Win_cen;
		for (; iCol <= iColMax; iCol++, puiSumCurr++)
		{
			iCmin = iCol - Win_cen;
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				pbBrightnessRow[iCol] = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
				{
					pbBrightnessRow[iCol] = u32ValueAdd = 255;
					pHistAdd = pHistAll + (255 * ar_cmmIn.m_i64W + iCmin);
				}
				else
				{
					pbBrightnessRow[iCol] = static_cast<uint8_t>(u32ValueAdd);
					pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W + iCmin);
				}// Добавляем яркость в Win_size гистограмм и сумм
				for (i = 0; i < Win_size; i++)
					puiSumCurr[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1));
			}
			if ((Win_size - 1) == iRow)
			{
				pfRowOut[0] = static_cast<float>(puiSumCurr[0] * Size_obratn);
				pfRowOut++;
			}
		}
		// Последние [ar_cmmIn.m_i64W - 2*Win_cen, ar_cmmIn.m_i64W - 1] колонки
		for (; iCol < ar_cmmIn.m_i64W; iCol++)
		{
			iCmin = iCol - Win_cen;
			iCmax = std::min(ar_cmmIn.m_i64W - Win_cen - 1, iCol + Win_cen);
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				pbBrightnessRow[iCol] = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
				{
					pbBrightnessRow[iCol] = u32ValueAdd = 255;
					pHistAdd = pHistAll + (255 * ar_cmmIn.m_i64W);
				}
				else
				{
					pbBrightnessRow[iCol] = static_cast<uint8_t>(u32ValueAdd);
					pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W);
				}// Добавляем яркость в Win_size гистограмм и сумм
				for (i = iCmin; i <= iCmax; i++)
					puiSum[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1));
			}
			if ((Win_size - 1) == iRow)
			{
				pfRowOut[0] = static_cast<float>(puiSum[iCmin] * Size_obratn);
				pfRowOut++;
			}
		}
	}
	// Последующие строки [Win_size, ar_cmmIn.m_i64H[
	for (iRow = Win_size; iRow < ar_cmmIn.m_i64H; iRow++)
	{
		// Обнуляем края строк.
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * Win_cen);
		memset(ar_cmmOut.pfGetRow(iRow) + (ar_cmmIn.m_i64W - Win_cen), 0, sizeof(float) * Win_cen);
		uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow);
		float* pfRowOut = ar_cmmOut.pfGetRow(iRow - Win_cen) + Win_cen;
		iPos = (iRow - Win_size) % Win_size;
		pbBrightnessRow = pbBrightness + (iPos * ar_cmmIn.m_i64W);
		// Первые [0, 2*Win_cen[ колонки
		for (iCol = 0; iCol < (Win_cen << 1); iCol++)
		{
			iCmin = std::max((long long)Win_cen, iCol - Win_cen);
			iCmax = iCol + Win_cen;
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				u32ValueAdd = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
					u32ValueAdd = 255;
			}
			if (u32ValueAdd != (u32ValueSub = pbBrightnessRow[iCol]))
			{
				pHistSub = pHistAll + (u32ValueSub * ar_cmmIn.m_i64W);
				pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W);
				pbBrightnessRow[iCol] = u32ValueAdd;
				// Добавляем яркость в Win_size гистограмм и сумм
				for (i = iCmin; i <= iCmax; i++)
					puiSum[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1)) -
					u32ValueSub * ((static_cast<uint32_t>(pHistSub[i]--) << 1) - 1);
			}
		}
		// Средние [2*Win_cen, ar_cmmIn.m_i64W - Win_size] колонки
		puiSumCurr = puiSum + Win_cen;
		pHist = pHistAll + (iCol - Win_cen);
		for (; iCol <= iColMax; iCol++, puiSumCurr++, pHist++)
		{
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
			//	при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				u32ValueAdd = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
					u32ValueAdd = 255;
			}
			if (u32ValueAdd != (u32ValueSub = pbBrightnessRow[iCol]))
			{
				pHistSub = pHist + (u32ValueSub * ar_cmmIn.m_i64W);
				pHistAdd = pHist + (u32ValueAdd * ar_cmmIn.m_i64W);
				pbBrightnessRow[iCol] = u32ValueAdd;

				uint32_t u32ValueAddSub = u32ValueAdd + u32ValueSub;
				// Добавляем яркость в Win_size гистограмм и сумм
				for (i = 0; i < Win_size; i++)
					puiSumCurr[i] += u32ValueAddSub + ((u32ValueAdd * static_cast<uint32_t>(pHistAdd[i]++) -
						u32ValueSub * static_cast<uint32_t>(pHistSub[i]--)) << 1);
			}
			pfRowOut[0] = static_cast<float>(puiSumCurr[0] * Size_obratn);
			pfRowOut++;
		}
		// Последние [ar_cmmIn.m_i64W - 2*Win_cen, ar_cmmIn.m_i64W - 1] колонки
		for (; iCol < ar_cmmIn.m_i64W; iCol++)
		{
			iCmin = iCol - Win_cen;
			iCmax = std::min(ar_cmmIn.m_i64W - Win_cen - 1, iCol + Win_cen);
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				u32ValueAdd = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
					u32ValueAdd = 255;
			}
			if (u32ValueAdd != (u32ValueSub = pbBrightnessRow[iCol]))
			{
				pHistSub = pHistAll + (u32ValueSub * ar_cmmIn.m_i64W);
				pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W);
				pbBrightnessRow[iCol] = u32ValueAdd;
				// Добавляем яркость в Win_size гистограмм и сумм
				for (i = iCmin; i <= iCmax; i++)
					puiSum[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1)) -
					u32ValueSub * ((static_cast<uint32_t>(pHistSub[i]--) << 1) - 1);
			}
			pfRowOut[0] = static_cast<float>(puiSum[iCmin] * Size_obratn);
			pfRowOut++;
		}
	}
	// Обнуляем края. Последние строки.
	for (iRow = ar_cmmIn.m_i64H - Win_cen; iRow < ar_cmmOut.m_i64H; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	if (nullptr != pHistAll)
		delete[] pHistAll;
	if (nullptr != puiSum)
		delete[] puiSum;
	if (nullptr != pbBrightness)
		delete[] pbBrightness;
}

void TextureFiltr_Mean_V8_3(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
}

void TextureFiltr_Mean_V8_3_21(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
}

void TextureFiltr_Mean_V8_3_sse(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
	double Size_obratn = 1.0 / 9;
	// Кэш для гистограмм
	uint16_t* pHistAll = new uint16_t[256 * ar_cmmIn.m_i64W]; // [256][ar_cmmIn.m_i64W]
	memset(pHistAll, 0, sizeof(uint16_t) * 256 * ar_cmmIn.m_i64W);
	uint16_t* pHist, * pHistAdd, * pHistSub;
	// Кэш для сумм
	uint32_t* puiSum = new uint32_t[ar_cmmIn.m_i64W], * puiSumCurr;
	memset(puiSum, 0, sizeof(uint32_t) * ar_cmmIn.m_i64W);
	// Кэш для яркостей
	uint8_t* pbBrightness = new uint8_t[ar_cmmIn.m_i64W * 3];
	uint8_t* pbBrightnessRow;
	int64_t iRow, iCol, iColMax = ar_cmmIn.m_i64W - 3;
	ar_cmmOut.iCreate(ar_cmmIn.m_i64W, ar_cmmIn.m_i64H, 6);
	// Обнуляем края. Первые строки.
	for (iRow = 0; iRow < 1; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	int64_t i, iCmin, iCmax, iPos = 0;
	uint32_t u32ValueAdd, u32ValueSub;
	double d;
	// Первые [0, 3 - 1] строки
	pbBrightnessRow = pbBrightness;
	float* pfRowOut = ar_cmmOut.pfGetRow(1) + 1;
	for (iRow = 0; iRow < 3; iRow++, pbBrightnessRow += ar_cmmIn.m_i64W)
	{
		// Обнуляем края строк.
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * 1);
		memset(ar_cmmOut.pfGetRow(iRow) + (ar_cmmIn.m_i64W - 1), 0, sizeof(float) * 1);
		uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow);
		// Первые [0, 2*1[ колонки
		for (iCol = 0; iCol < (1 << 1); iCol++)
		{
			iCmin = std::max((long long)1, iCol - 1);
			iCmax = iCol + 1;
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				pbBrightnessRow[iCol] = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
				{
					pbBrightnessRow[iCol] = u32ValueAdd = 255;
					pHistAdd = pHistAll + (255 * ar_cmmIn.m_i64W);
				}
				else
				{
					pbBrightnessRow[iCol] = static_cast<uint8_t>(u32ValueAdd);
					pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W);
				}
				// Добавляем яркость в 3 гистограмм и сумм
				for (i = iCmin; i <= iCmax; i++)
					puiSum[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1));
			}
		}
		// Средние [2*1, ar_cmmIn.m_i64W - 3] колонки
		puiSumCurr = puiSum + 1;
		for (; iCol <= iColMax; iCol++, puiSumCurr++)
		{
			iCmin = iCol - 1;
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				pbBrightnessRow[iCol] = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
				{
					pbBrightnessRow[iCol] = u32ValueAdd = 255;
					pHistAdd = pHistAll + (255 * ar_cmmIn.m_i64W + iCmin);
				}
				else
				{
				pbBrightnessRow[iCol] = static_cast<uint8_t>(u32ValueAdd);
				pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W + iCmin);
				}
				// Добавляем яркость в 3 гистограмм и сумм
				for (i = 0; i < 3; i++)
					puiSumCurr[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1));
			}
			if((3 - 1) == iRow)
			{
				pfRowOut[0] = static_cast<float>(puiSumCurr[0] * Size_obratn);
				pfRowOut++;
			}
		}
		// Последние [ar_cmmIn.m_i64W - 2*1, ar_cmmIn.m_i64W - 1] колонки
		for (; iCol < ar_cmmIn.m_i64W; iCol++)
		{
			iCmin = iCol - 1;
			iCmax = std::min(ar_cmmIn.m_i64W - 1 - 1, iCol + 1);
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				pbBrightnessRow[iCol] = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
				{
					pbBrightnessRow[iCol] = u32ValueAdd = 255;
					pHistAdd = pHistAll + (255 * ar_cmmIn.m_i64W);
				}
				else
				{
				pbBrightnessRow[iCol] = static_cast<uint8_t>(u32ValueAdd);
				pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W);
				}
				// Добавляем яркость в 3 гистограмм и сумм
				for (i = iCmin; i <= iCmax; i++)
					puiSum[i] += u32ValueAdd * (1 + (static_cast<uint32_t>(pHistAdd[i]++) << 1));
			}
			if((3 - 1) == iRow)
			{
				pfRowOut[0] = static_cast<float>(puiSum[iCmin] * Size_obratn);
				pfRowOut++;
			}
		}
	}
	__m128i xmmHistOne_3 = _mm_set_epi16(0, 0, 0, 0, 0, 1, 1, 1); // Для inc или dec сразу для 3 гистограмм
	// Последующие строки [3, ar_cmmIn.m_i64H[
	for (iRow = 3; iRow < ar_cmmIn.m_i64H; iRow++)
	{
		// Обнуляем края строк.
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * 1);
		memset(ar_cmmOut.pfGetRow(iRow) + (ar_cmmIn.m_i64W - 1), 0, sizeof(float) * 1);
		uint16_t* pRowIn = ar_cmmIn.pu16GetRow(iRow);
		float* pfRowOut = ar_cmmOut.pfGetRow(iRow - 1) + 1;
		iPos = (iRow - 3) % 3;
		pbBrightnessRow = pbBrightness + (iPos * ar_cmmIn.m_i64W);
		// Первые [0, 2*1[ колонки
		for (iCol = 0; iCol < (1 << 1); iCol++)
		{
			iCmin = std::max((long long)1, iCol - 1);
			iCmax = iCol + 1;
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				u32ValueAdd = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
					u32ValueAdd = 255;
			}
			if (u32ValueAdd != (u32ValueSub = pbBrightnessRow[iCol]))
			{
				pHistSub = pHistAll + (u32ValueSub * ar_cmmIn.m_i64W);
				pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W);
				pbBrightnessRow[iCol] = u32ValueAdd;
				uint32_t u32ValueAddSub = u32ValueAdd + u32ValueSub;
				// Добавляем яркость в 3 гистограмм и сумм
				for (i = iCmin; i <= iCmax; i++)
					puiSum[i] += u32ValueAddSub + ((u32ValueAdd * static_cast<uint32_t>(pHistAdd[i]++) -
						u32ValueSub * static_cast<uint32_t>(pHistSub[i]--)) << 1);
			}
		}
		// Средние [2*1, ar_cmmIn.m_i64W - 3] колонки
		puiSumCurr = puiSum + 1;
		pHist = pHistAll + (iCol - 1);
		for (; iCol <= iColMax; iCol++, puiSumCurr++, pHist++)
		{
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				u32ValueAdd = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
					u32ValueAdd = 255;
			}
			if (u32ValueAdd != (u32ValueSub = pbBrightnessRow[iCol]))
			{
				pHistSub = pHist + (u32ValueSub * ar_cmmIn.m_i64W);
				pHistAdd = pHist + (u32ValueAdd * ar_cmmIn.m_i64W);
				pbBrightnessRow[iCol] = u32ValueAdd;
				// Добавляем яркость в 3 гистограмм и сумм
				//============================
				// Первые 3 точки
				//============================
				__m128i xmmHistSub = _mm_lddqu_si128(reinterpret_cast<__m128i*>(pHistSub)); //
				//Загружаем 128 бит(используем 48 бит - 3 эл.массива)
				__m128i xmmHistAdd = _mm_lddqu_si128(reinterpret_cast<__m128i*>(pHistAdd));
				// 3 элемента гистограммы
				__m128i xmmSumCurr = _mm_lddqu_si128(reinterpret_cast<__m128i*>(puiSumCurr)); //
				//Загружаем 128 бит(используем 96 бит - 3 эл.массива)
				__m128i xmmHistSub32 = _mm_cvtepu16_epi32(xmmHistSub); // HistSub32[3], HistSub32[2],
				//HistSub32[1], HistSub32[0](Hi < ->Lo)
				__m128i xmmHistAdd32 = _mm_cvtepu16_epi32(xmmHistAdd);
				__m128i xmmValueAdd = _mm_loadu_si32(&u32ValueAdd); // 0, 0, 0, u32ValueAdd (Hi
				//< ->Lo)
				__m128i xmmValueSub = _mm_loadu_si32(&u32ValueSub);
				xmmValueAdd = _mm_castps_si128(_mm_shuffle_ps(_mm_castsi128_ps(xmmValueAdd), _mm_castsi128_ps(xmmValueAdd), 0x0C0)); // 0, u32ValueAdd, u32ValueAdd, u32ValueAdd
				xmmValueSub = _mm_castps_si128(_mm_shuffle_ps(_mm_castsi128_ps(xmmValueSub), _mm_castsi128_ps(xmmValueSub), 0x0C0));
				xmmHistAdd32 = _mm_sub_epi32(_mm_mullo_epi32(xmmHistAdd32, xmmValueAdd),
					_mm_mullo_epi32(xmmHistSub32, xmmValueSub));
				xmmHistAdd32 = _mm_add_epi32(xmmHistAdd32, xmmHistAdd32); // <<= 1
				xmmHistAdd32 = _mm_add_epi32(xmmHistAdd32, xmmValueAdd);
				xmmHistAdd32 = _mm_add_epi32(xmmHistAdd32, xmmValueSub);
				_mm_storeu_si128(reinterpret_cast<__m128i*>(puiSumCurr), _mm_add_epi32(xmmSumCurr,
					xmmHistAdd32)); // Сохраняем puiSumCurr[0,1,2,3]
				_mm_storeu_si128(reinterpret_cast<__m128i*>(pHistAdd), _mm_add_epi16(xmmHistAdd,
					xmmHistOne_3)); // pHistAdd[0-2]++; Сохраняем 128 бит (используем 128 - 8 эл. массива)
				_mm_storeu_si128(reinterpret_cast<__m128i*>(pHistSub), _mm_sub_epi16(xmmHistSub,
					xmmHistOne_3)); // pHistSub[0-2]--; Сохраняем 128 бит (используем 128 - 8 эл. массива)
			}
			pfRowOut[0] = static_cast<float>(puiSumCurr[0] * Size_obratn);
			pfRowOut++;
		}
		// Последние [ar_cmmIn.m_i64W - 2*1, ar_cmmIn.m_i64W - 1] колонки
		for (; iCol < ar_cmmIn.m_i64W; iCol++)
		{
			iCmin = iCol - 1;
			iCmax = std::min(ar_cmmIn.m_i64W - 1 - 1, iCol + 1);
			if ((d = (pRowIn[iCol] - pseudo_min) * kfct) < 0.5) // Так как при яркости 0 вклад в сумму будет 0
				//при любом pHist[0], то pHist[0] можно просто нигде не учитывать, но яркость в кэше нужно обнулить!
				u32ValueAdd = 0;
			else
			{
				if ((u32ValueAdd = static_cast<uint32_t>(d + 0.5)) > 255)
					u32ValueAdd = 255;
			}
			if (u32ValueAdd != (u32ValueSub = pbBrightnessRow[iCol]))
			{
				pHistSub = pHistAll + (u32ValueSub * ar_cmmIn.m_i64W);
				pHistAdd = pHistAll + (u32ValueAdd * ar_cmmIn.m_i64W);
				pbBrightnessRow[iCol] = u32ValueAdd;
				uint32_t u32ValueAddSub = u32ValueAdd + u32ValueSub;
				// Добавляем яркость в 3 гистограмм и сумм
				for (i = iCmin; i <= iCmax; i++)
					puiSum[i] += u32ValueAddSub + ((u32ValueAdd * static_cast<uint32_t>(pHistAdd[i]++) -
						u32ValueSub * static_cast<uint32_t>(pHistSub[i]--)) << 1);
			}
			pfRowOut[0] = static_cast<float>(puiSum[iCmin] * Size_obratn);
			pfRowOut++;
		}
	}
	// Обнуляем края. Последние строки.
	for (iRow = ar_cmmIn.m_i64H - 1; iRow < ar_cmmOut.m_i64H; iRow++)
		memset(ar_cmmOut.pfGetRow(iRow), 0, sizeof(float) * ar_cmmOut.m_i64LineSizeEl);
	if (nullptr != pHistAll)
		delete[] pHistAll;
	if (nullptr != puiSum)
		delete[] puiSum;
	if (nullptr != pbBrightness)
		delete[] pbBrightness;
}


void TextureFiltr_Mean_V8_21_sse(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
}

void TextureFiltr_Mean_V8_sse4_3_21(MU16Data & ar_cmmIn, MFData & ar_cmmOut, int Win_cen, double pseudo_min, double kfct)
{
}

